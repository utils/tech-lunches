# Introduction to Ghostflow

Tech lunch introducing Ghostflow, the implementation of the Kitware Robot
managing the workflow for CMake, VTK, ParaView, and more.

# Attributions

CSS and configuration based on a previous Kitware talk at:

    https://github.com/rcos/CSCI2961-01/tree/master/Lectures/Lecture-Build-Systems
