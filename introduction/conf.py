import sys
import os

extensions = [
    'hieroglyph',
]

source_suffix = '.rst'
master_doc = 'index'

project = u'Introduction to Ghostflow'
copyright = u'2017, Kitware, Inc.'
author = u'Ben Boeckel'

exclude_patterns = ['_build']
templates_path = ['templates']
raw_enabled = True

pygments_style = 'sphinx'

todo_include_todos = True

html_theme = 'alabaster'
html_title = project
html_static_path = ['static']

slide_title = 'Ghostflow and you'
slide_theme = 'slides'
slide_numbers = True
slide_footer = '''
<a class="slide-footer-image" href="http://creativecommons.org/licenses/by-sa/4.0/">
  <img border="0" src="_static/cc-by-sa.png" alt="CC BY-SA" />
</a>
<span class="slide-footer-text">
  Kitware Tech Lunch - Spring 2017 - Introduction to Ghostflow - Kitware, Inc.
</span>
'''

slide_theme_options = {
    'custom_css': 'custom.css',
    'custom_js': 'custom.js',
}
